#include<windows.h>
#include<stdio.h>
#define PI 3.14159265354
double circle(int);
double area(int);
double volume(int);
int main(int argc,char **argv){
int r;
printf("输入半径：");
scanf("%d",&r);
printf("周长是：%f\n",circle(r));
printf("面积是：%f\n",area(r));
printf("体积是：%f\n",volume(r));
system("pause");
    return 0;
}
double circle(int r){
    return PI*2*r;
}
double area(int r){
    return PI*r*r;
}
double volume(int r){
    return PI*r*r*r*4/3;
}